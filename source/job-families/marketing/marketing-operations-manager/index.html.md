---
layout: job_family_page
title: "Marketing Operations Manager"
---

You understand that setting a foundation for healthy growth in a fast-paced company is effective marketing operations. In all cases, you share our [values](https://about.gitlab.com/handbook/values/)
*  [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)

## Associate Marketing Operations Manager
Your job is to support the Marketing Operations team in meeting our goals of optimizing technology to enable effective marketing at GitLab, ensuring high data quality and helping colleagues access that datat to enable smarter decisions, and assisting in marketing analysis, planning and strategy.  This is an entry level position for Marketing Operations.

## Responsibilities

Marketing technology
*  Learning tools that marketing has in place to act as backup on various systems as needed
*  Provisioning licenses for base entitlements to appropriate GitLab team members
*  Providing oversight on GitLab issues boards - building weekly standup issues, creating biweekly milestone issues to ensure handbook updates are completed, documenting minutes from meetings, and ensuring lables exist on all Marketing issues

Marketing data stewardship
*  Assisting with data cleanliness in the various systems, merging like records, researching cuases of bugs in order to correct processes
*  Cleansing, enriching and uploading prospect lists from various events
*  Building smart lists in Marketo to aid in geographic and account based marketing efforts

Marketing analysis
*  Providing support to team with ad hoc analysis and/or the underlying data as needed

## Requirements

*  Strong spoken and written English.
*  Experience in sales and/or marketing teams of B2B software, Open Source software, and the developer tools space is preferred.
*  Experience with marketing automation software a plus
*  Experience with Salesforce CRM software helpful
*  Familiarity with Git and respositories useful
*  Proficiency in MS Excel/ Google Sheets
*  You are team-centric
*  You're a self starter, willing to read and watch in order to learn.  (Be ready to learn and how to use GitLab and Git)
*  Ability to use GitLab

## Marketing Operations Manager (Intermediate)

Your job is three fold: evaluate/select/customize technology to enable effective marketing at GitLab, ensure high quality and help colleagues access that data to enable smarter decisions, and assist in marketing analysis/planning/strategy.  Within the Marketing Operations Manager role, you may be focused on a specific area of marketing (i.e. Sales Development, Events/Campaigns, Digital, etc.), infrastructure or data analysis depending on the needs of the team.

## Responsibilities

Marketing technology
*  Optimize existing marketing technology and the entire tool stack
*  Evaluate new marketing technology that can enable GitLab to grow its business faster and more efficiently.
*  Create documentation that guides the marketing team in their use of marketing software tools.
*  Train the marketing team on marketing software tools.
*  Audit use of marketing software tools with an eye towards continually improving how they are configured.

Marketing data stewardship
*  Help determine and document processes across marketing that facilitate efficiency, effectiveness, and accurate data collection.
*  Review data quality across key dimensions that GitLab uses to evaluate its marketing performance.
*  Where data quality is lacking, identify the root cause and address systematically with improved processes.

Marketing analysis
*  Measure the marketing department's contribution to the sales pipeline, and assess their performance throughout the entire funnel.
*  Measure the effectiveness of marketing campaigns and content, including ROI of marketing campaigns.
*  Measure the ratio of customer acquisition cost to customer lifetime value, by marketing tactic.
*  Assist with data-driven budgeting, planning, and strategy

## Requirements

All of the above requirements and:
*  Two plus years of experience in Marketing Operations or related role.
*  Bachelor's degree.  Is your college degree in French foreign politics with a minor in interpretive dance but you've been selling and marketing products since you were 12!  We understand that your degree isn't the only thing that perpares you as a potential job candidate.
*  Experience with modern marketing and sales development solutions such as LeanData, Bizible, and Outreach.
*  Experience in sales and/or marketing teams of B2B software, Open Source Software, and the developer tools space is preferred.
*  Marketo or other modern marketing automation system experience required.
*  Salesforce or other modern CRM tool experience required.
*  You are obsessed with making customers happy.  You know that the slightest trouble in getting started with a product can ruin customer happiness.
*  Be ready to learn how to use GitLab and Git.

## Senior Marketing Operations Manager

## Responsibilities

All of the above responsibilities and:
*  Lead strategic and operational initiatives across marketing and with other functions to drive performance improvements, continuously enhance the impact of marketing and help the company continue along its fast growth trajectory.
*  Collaborate across functions to structure problems, conduct analyses, and drive to solutions through a rigourous, data-driven process.
*  Create and manage project plans with clearly defined deliverables and resources, coordinate work streams and dependencies, track and communicate progress, and identify obstacles and ensure they are addressed.

## Requirements

All of the above requirements and:
*  Excellent spoken and written English.
*  5+ years of experience, including more technical expertise.
*  Excellent analytical and problem-solving skills.
*  Strong oral and written communication skills, including ability to concisely present project deliverables.
*  Ability to work successfully with little guidance.

## Manager, Marketing Operations

## Responsibilities

*  Effectively influence, align, and colloborate with key functions, particularly Sales, Finance, Compliance, and Legal.
*  Recruit, develop and lead a team to execute on key marketing strategies.

## Requirements

*  8+ years of experience with go-to-market operations strategy and business analytics.
*  5+ years of experience managing marketing vendors and/or team management.
*  Ability to work collaboratively, internally and externally.
*  Ability to manager/prioritize multiple projects and adapt to a changing, fast-paced environment.
*  Demonstrated management skill and ability to lead a team and work in a group environment, including interaction with Senior/Executive level leadership.
*  Possess strong interpersonal skills including influencing, negotiations and teamwork skills.
*  Ability to think strategically, develop frameworks and platforms that ensure optimal and unfettered access to business tools, assets and capabilities.
*  Superb analytical, problem-solving capability and help the team make sound decisions.
*  You believe in teamwork and are not afraid of rolling up your sleeves.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).
* Qualified candidates will be invited to schedule a 15 minute [screening call](/handbook/hiring/interviewing/#conducting-a-screening-call) with one of our Global Recruiters after completing our questionnaire provided.
* A 30 minute interview with future co-worker/s (Marketing Operations Manager)
* A 45 minute interview with future manager (Director, Marketing Operations)
* A 30 minute interview with future marketing partner/s based on specialization
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Relevant Links
[Marketing Handbook](/handbook/marketing)
