---
layout: markdown_page
title: "Category Direction - Importers"
---

- TOC
{:toc}

Last Reviewed: 2020-02-27

## Importers

Thanks for visiting the direction page for Importers in GitLab. If you'd like to provide feedback on this page or contribute to this vision, please feel free to open a merge request for this page or comment in the [corresponding epic](https://gitlab.com/groups/gitlab-org/-/epics/2248) for this category.

## Problems to solve

The mission of the Importers category is to provide a great experience importing from other applications in our customer's DevOps toolchains. This also includes GitLab-to-GitLab migrations, particularly self-managed GitLab to GitLab.com. 
* Note: Other groups may lead on individual importers based on their strategic importance, but Import is responsible for maintaining a great import/export framework, maintaining an importer strategy that prioritizes our competitors, and prioritizes work on specific importers. In particular, our Jenkins and Jira importers are currently being delivered by our Verify and Plan stage teams respectively. Source code importers continue to be fully delivered by the Import group.

A typical organization looking to adopt GitLab already has many other tools. Artifacts such as code, build pipelines, issues and roadmaps may already exist and are being used daily. Seamless transition of work in progress is critically important and a great experience during this migration creates a positive first impression of GitLab. 

## What's next & why

At the moment, the Import group is focused on enabling GitLab.com adoption through the introduction of [group import/export](https://gitlab.com/groups/gitlab-org/-/epics/1952). Additionally, our UX group is looking into the overall import user experience in order to create a more positive first impression when migrating to GitLab.

While this group focuses on building importers, it can often happen that importers are a higher priority for another group in gaining adoption of their features. When this happens other teams should not wait for their importer to become a priority of the Import group, but should just prioritize the work themselves since [everyone can contribute](https://about.gitlab.com/handbook/values/#mission).

## Maturity plan

Our maturity plan is currently under construction. If you'd like to contribute feedback on areas you'd like to see prioritized, please add them as comments in the [epic](https://gitlab.com/groups/gitlab-org/-/epics/2248).
