---
layout: handbook-page-toc
title: Sentry
category: Infrastructure for troubleshooting
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Sentry

This [deck](https://docs.google.com/presentation/d/1j1J4NhGQEYBY8la6lCK-N-bw749TbcTSFTD-ANHiels/edit#slide=id.p) (GitLab internal only) provides an introduction to using Sentry for tracking errors in GitLab.
